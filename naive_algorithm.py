import sys
import time
import random
from collections import deque
from itertools import combinations
from copy import deepcopy
from utilities_functions import *

INFINI = - sys.maxint - 1

def naive_algorithm(D, M, i):
    d = INFINI
    j = 0
    R = [v for v in D.keys() if v not in M]
    for E in combinations(R, i):
        M_prim = deepcopy(M)
        M_prim.extend(E)
        I = induced_graph(D, M_prim)
        if is_acyclic(I):
            d_temp = delta(I)
            if d_temp > d:
                d = d_temp
    return d

path = sys.argv[1]
M = sys.argv[2].split(',')
D = load_graph(path)
for i in range(len(D) - len(M)):
    start = time.time()
    d = naive_algorithm(D, M, i)
    end = time.time()
    print(str(i + len(M)) + ',' + str(d) + ',' + str(end -start))