def load_graph(path):
    f = open(path, 'r').readlines()
    n, m = f[0].split()
    D = {str(i) : [] for i in range(1, int(n) + 1)}
    for i in range(1, len(f)):
        u, v, p = f[i].split()
        D[u].append(v)
    return D

def delta(I):
    I_plus = {u : 0 for u in I.keys()}
    I_moins = {u : 0 for u in I.keys()}
    p = 0
    s = 0
    for u, ajd in I.items():
        for v in ajd:
            I_plus[u] += 1
            I_moins[v] += 1
    for u in I_plus.keys():
        if I_plus[u] == 0 and I_moins[u] > 0:
            p += 1
        elif I_plus[u] > 0 and I_moins[u] == 0:
            s += 1
    return p - s

def induced_graph(D, V):
    I = {u : [] for u in V}
    for u in V:
        for v in V:
            if v in D[u]:
                I[u].append(v)
    return I

def circuit(I, v, M, P):
    M.append(v)
    P.append(v)
    for u in I[v]:
        if not u in M:
            if circuit(I, u, M, P):
                return True
        elif u in P:
            return True
    P.pop()
    return False

def is_acyclic(I):
    M = []
    P = []
    for v in I.keys():
        if not v in M:
            if circuit(I, v, M, P):
                return False
    return True