import sys
import random
from collections import deque
from itertools import combinations
from copy import deepcopy
from utilities_functions import *

INFINI = - sys.maxint - 1

def rank_vertices(D, I):
    V_I = deepcopy(I.keys())
    d_I = delta(I)
    E_S = { u : INFINI for u in set(D.keys()).difference(set(V_I))}
    for u in E_S.keys():
        V_I.append(u)
        I_prim = induced_graph(D, V_I)
        if is_acyclic(I_prim):
            E_S[u] = delta(I_prim) - d_I
        V_I.pop()
    return E_S

def greedy_algorithm(D, i):
    I = {random.choice(D.keys()): []}
    while len(I) < i:
        E_S = rank_vertices(D, I)
        u = max(E_S, key = E_S.get)
        if E_S[u] != INFINI:
            V = I.keys()
            V.append(u)
            I = induced_graph(D, V)
        else:
            return INFINI, None
    return delta(I), I
 
def neighbors_vertices(D, I):
    diff = list(set(D.keys()).difference(set(I.keys())))
    M = set()
    for u in diff:
        for v, adj in D.items():
            if v in I.keys() and u in adj:
                M.add(u)
    return list(M)

def neighborhood(D, I, u):
    for v in I.keys():
        V_prim = deepcopy(I.keys())
        V_prim.remove(v)
        V_prim.append(u)
        I_prim = induced_graph(D, list(set(V_prim)))
        if is_acyclic(I_prim):
            yield I_prim, v

def tabu_search(D, I):
    I_prim = deepcopy(I)
    B = deepcopy(I)
    T = deque([], maxlen=2)
    NB_ITERATIONS = 3 * len(D)
    i = 0
    keep_looping = True
    S = []
    while keep_looping:
        exit = False
        N = neighbors_vertices(D, I_prim)
        if not N:
            break
        for u in N:
            if not u in T:
                for I_pp, v in neighborhood(D, I_prim, u):
                    if delta(I_prim) < delta(I_pp):
                        T.append(v)
                        I_prim = deepcopy(I_pp)
                        if delta(B) < delta(I_pp):
                            B = deepcopy(I_pp)
                        exit = True
                        break
                if exit:
                    break
                T.append(u)
        v = T[0]
        for I_pp, w in neighborhood(D, I_prim, v):
            if delta(B) < delta(I_pp):
                B = deepcopy(I_pp)
        i += 1
        keep_looping = B != I_prim and i < NB_ITERATIONS
    return delta(B), B

def random_strategy(D, i, s):
    R = []
    K = D.keys()
    random.shuffle(K)
    for E in combinations(K, i):
        R.append(list(E))
        if len(R) == s:
            break
    return R

def zero_deg(X):
    for v in X.keys():
        if neg_deg(v, X) == 0:
            return v
    return None

def del_vertex(v, X):
    if v in X:
        del X[v]
    for val in X.values():
        if v in val:
            val.remove(v)

def neg_deg(v, X):
    deg = 0
    for val in X.values():
        if v in val:
            deg += 1
    return deg

def max_deg(X):
    d_max = X.keys()[0]
    for key, val in X.items():
        if len(val) > len(X[d_max]):
            d_max = key
    return d_max

def strategy_cost(S, X):
    cost = 0
    while len(S) > 0 and len(X) > 0:
        l = S[0]
        del S[0]
        del_vertex(l, X)
        cost += 1
        deg = zero_deg(X)
        while deg != None:
            del_vertex(deg, X)
            deg = zero_deg(X)

    return cost, X

def complete_cost(S, X):
    cost, X_prim = strategy_cost(S, X)
    while len(X) > 0:
        l = max_deg(X)
        del_vertex(l, X)
        cost += 1
        deg = zero_deg(X)
        while deg != None:
            del_vertex(deg, X)
            deg = zero_deg(X)
    return cost

path = sys.argv[1]
N = int(sys.argv[2])
D = load_graph(path)

cor = { "1" : "accomplish", "2" : "action", "3" : "action", "4" : "apart", "5" : "condition", "6" : "detail", "7" : "different", "8" : "do", "9" : "else", "10" : "general", "11" : "have", "12" : "importance", "13" : "important", "14" : "instead", "15" : "like", "16" : "make", "17" : "mention", "18" : "need", "19" : "other", "20" : "own", "21" : "particular", "22" : "place", "23" : "position", "24" : "possess", "25" : "possession", "26" : "process", "27" : "process", "28" : "produce", "29" : "qualify", "30" : "quality", "31" : "refer", "32" : "similar", "33" : "something", "34" : "state", "35" : "succeed", "36" : "success", "37" : "thing", "38" : "thing", "39" : "not", "40" : "negative"}

for i in range(14, 27):
    res = str(i)
    opt_cost = 45
    f = open('random_strategies_' + str(i), 'w')
    for j in range(1, 11):
        d_greedy, I = greedy_algorithm(D, i)
        R = random_strategy(D, i, N)
        avg_cost = 0
        f.write('Test number : ' + str(j) + '\n')
        for E in R:
            f.write(str([cor[k] for k in E]) + '\n')
            X = deepcopy(D)
            cost = complete_cost(E, X)
            avg_cost += cost
        d_tabu, B = tabu_search(D, I)
        X = deepcopy(D)
        cost = complete_cost(B.keys(), X)
        if cost < opt_cost:
            opt_cost = cost
        res += ',' + str(avg_cost / float(N))
    f.close()
    print(res + ',' + str(opt_cost))
